const categoriesDataSet = require('./categoriesMockupData');
const Category = require('./Category');

const categories = categoriesDataSet.map(catData => new Category(catData));

module.exports = {
    getCategory(id) {
        return categories.find(cat => cat.id === id);
    }
};