// just to have toArray() mock method, because Node.js does not have SFCC "List" class
function List(arrayData) {
    this.arrayData = arrayData;
}

List.prototype = {
    toArray() {
        return this.arrayData;
    }
};

module.exports = List;