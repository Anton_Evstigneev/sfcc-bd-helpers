const List = require('./List');
const constants = require('./constants');
const categoriesDataSet = require('./categoriesMockupData');

function Category(catData) {
    this.id = catData.id;
    this.parent = catData.parent;
}

Category.prototype = {
    getParent() {
        return categories.find(cat => cat.id === this.parent);
    },
    getSubCategories() {
        return new List(categories.filter(cat => cat.parent === this.id));
    },
    isRoot() {
        return this.id === constants.rootCategoryId;
    },
    isOnline() {
        // don't care about online status in the current context, assuming all categories are online
        return true;
    },
    getID() {
        return this.id;
    },
    getDisplayName() {
        // don't care about display name in the current context, let it be the same as ID
        return this.id;
    }
};

// define categories list AFTER we define Category constructor
const categories = categoriesDataSet.map(catData => new Category(catData));

module.exports = Category;