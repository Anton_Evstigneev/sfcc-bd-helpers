module.exports = {
    https(controllerPath, ...URLParams) {
        let paramsList = [];
        if (URLParams.length % 2 !== 0) {
            throw new Error ('each URL parameter must have value');
        }

        for (let i = 0; i < URLParams.length; i += 2) {
            paramsList.push(URLParams[i] + '=' + URLParams[i + 1]);
        }

        return controllerPath + '?' + paramsList.join('&');
    }
};