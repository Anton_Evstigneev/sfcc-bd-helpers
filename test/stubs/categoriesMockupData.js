const constants = require('./constants');

const categoriesDataSet = [
    {
        id: constants.rootCategoryId,
        parent: null
    }, {
        id: 'a1',
        parent: constants.rootCategoryId
    }, {
        id: 'a2',
        parent: constants.rootCategoryId
    }, {
        id: 'a3',
        parent: constants.rootCategoryId
    }, {
        id: 'a11',
        parent: 'a1'
    }, {
        id: 'a12',
        parent: 'a1'
    }, {
        id: 'a111',
        parent: 'a11'
    }, {
        id: 'a112',
        parent: 'a11'
    }, {
        id: 'a21',
        parent: 'a2'
    }, {
        id: 'a211',
        parent: 'a21'
    }, {
        id: 'a31',
        parent: 'a3'
    }
];

module.exports = categoriesDataSet;