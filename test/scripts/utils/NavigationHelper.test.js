// libs
const chai = require('chai'),
    mockRequire = require('mock-require');

// stubs
const CatalogMgr = require('../../stubs/CatalogMgr'),
    URLUtils = require('../../stubs/URLUtils');

// creating SFCC API mockups
mockRequire('dw/catalog/CatalogMgr', CatalogMgr);
mockRequire('dw/web/URLUtils', URLUtils);

// testing target
const NavigationHelper = require('../../../scripts/utils/NavigationHelper');

describe('NavigationHelperTest', function() {
    it('should return proper category path list for a given category', function() {
        const cases = [
            {
                catID: 'a211',
                restCatIDsList: ['a2', 'a21', 'a211'],
            }, {
                catID: 'a1',
                restCatIDsList: ['a1'],
            }
        ];

        cases.forEach(currentCase => {
            const resPathArray = NavigationHelper.getNavigationMatrix(currentCase.catID, false);

            if (resPathArray.length !== currentCase.restCatIDsList.length) {
                return false;
            }

            currentCase.restCatIDsList.forEach((pathItem, pathItemIndex) => {
                if (pathItem !== resPathArray[pathItemIndex].id) {
                    return false;
                }
            });
        });

        return true;
    });

    it('should return exactly one active category per path list item', function() {
        const resPathArray = NavigationHelper.getNavigationMatrix('a211', true);

        resPathArray.forEach(pathItem => {
            let activeCatCount = 0;

            pathItem.forEach(cat => {
                if (cat.isCategoryInActivePath) {
                    activeCatCount++;
                }
            });

            if (activeCatCount !== 1) {
                return false;
            }
        });

        return true;
    });

    it('should return each category data in proper format', function () {
        const resPathArray = NavigationHelper.getNavigationMatrix('a211', false);

        resPathArray.forEach(pathItem => {
            chai.expect(pathItem[0]).to.have.keys('cat', 'id', 'name', 'url', 'isCategoryInActivePath')
        });
    });

    it('should return root category sub-categories array if active category ID is not provided', function () {
        const res = NavigationHelper.getNavigationMatrix(null, true);
        return res.length === 1
            && res[0].filter(cat => ['a1', 'a2', 'a3'].indexOf(cat.ID) !== -1).length === 3;
    });
});