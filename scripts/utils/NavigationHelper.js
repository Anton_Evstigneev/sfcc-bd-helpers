'use strict';

/**
 * @module scripts/util/Navigation
 * @description Helper for gathering current navigation data in a format suitable for rendering in templates
 */

// API includes
var CatalogMgr = require('dw/catalog/CatalogMgr');
var URLUtils = require('dw/web/URLUtils');

var NavigationHelper = {
    _getCategoryUrl: function _getCategoryUrl(category) {
        return URLUtils.https('Search-Show', 'cgid', category.getID());
    },
    /**
     * @description General check if category should be included in navigation data structure
     * @param category
     * @return {Boolean}
     * @private
     */
    _isCategoryNavigatable: function _isCategoryNavigatable(category) {
        return category && category.isOnline();
    },
    _createCategoryItem: function _createCategoryItem(currentCategory, activeCategory) {
        var categoryItem = {
            cat: currentCategory,
            // easy debugging
            id: currentCategory.getID(),
            name: currentCategory.getDisplayName(),
            url: this._getCategoryUrl(currentCategory).toString()
        };

        if (currentCategory === activeCategory) {
            categoryItem.isCategoryInActivePath = true;
        }

        return categoryItem;
    },
    _getNavigationMatrixDefault: function _getNavigationMatrixDefault() {
        var topCategories = CatalogMgr.getCategory('root').getSubCategories().toArray();

        return topCategories.filter(function (cat) {
            return NavigationHelper._isCategoryNavigatable(cat);
        }).map(function (cat) {
            return NavigationHelper._createCategoryItem(cat);
        });
    },
    _getNavigationMatrixForCategory: function _getNavigationMatrixForCategory(activeCategoryID, areSiblingsIncluded) {
        var currentCategory = CatalogMgr.getCategory(activeCategoryID);
        var navigationMatrix = [];

        // we will reverse category tree "depth" later, by reversing the first dimension of navigation matrix
        var depth = 0;

        var navMatrixStep = function (cat) {
            if (NavigationHelper._isCategoryNavigatable(cat)) {
                navigationMatrix[depth].push(NavigationHelper._createCategoryItem(cat, currentCategory));
            }
        };

        while (!currentCategory.isRoot()) {
            if (!navigationMatrix[depth]) {
                navigationMatrix[depth] = [];
            }

            var parentCategory = currentCategory.getParent();
            var siblingCategories = parentCategory.getSubCategories().toArray();

            if (areSiblingsIncluded) {
                siblingCategories.forEach(navMatrixStep);
            } else {
                navMatrixStep(currentCategory);
            }

            depth++;
            currentCategory = parentCategory;
        }

        // to have root category at the start of the array
        return navigationMatrix.reverse();
    },
    /**
     * Transforms a category n-depth tree into a n-length array by selecting items for each depth following active category route.
     * Returns a list of category levels, each level contains category that is in current "active category"
     * direct route(marked as active with "isCategoryInActivePath" flag), and, optionally, all it's siblings.
     *
     * With sibling this method is useful for rendering column-based navigation that is common on many projects.
     * Without siblings this method is useful for rendering breadcrumbs
     *
     * If activeCategoryID is not provided- returns a list with only one element - subcategories list of the "root" category
     * @param activeCategoryID - active category ID
     * @return {Array}
     */
    getNavigationMatrix: function getNavigationMatrix(activeCategoryID, areSiblingsIncluded) {
        if (activeCategoryID) {
            return this._getNavigationMatrixForCategory(activeCategoryID, areSiblingsIncluded);
        }
        // return empty list for root category if activeCategoryID is not provided and areSiblingsIncluded is false
        if (!areSiblingsIncluded) {
            return [];
        }
        return this._getNavigationMatrixDefault(areSiblingsIncluded);
    }
};

module.exports = NavigationHelper;