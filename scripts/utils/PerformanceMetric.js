'use strict';

/**
 * @module scripts/util/PerformanceMetric
 * @description Simple time performance logger that allows to log execution time of arbitrary blocks of code, including total
 * execution time of function/method calls in a loops. This is very useful if you need to compare total block execution time
 *  to total controller execution time for example (to determine the performance bottleneck).
 * @constructor
 * Warning: be consistent in calling startMeasurement and endMeasurement methods. If each of them is called twice in a row
 * you will not receive any warnings.
 *
 * Usage:
 * var pm = PerformanceMetric.CreatePerformanceTimeMetric();
 * var loopTimeMetricId = 'loop over large list';
 * pm.startMeasurement(loopTimeMetricId);
 * {loop over it, do some heavy calculation, whatever...}
 * pm.endMeasurement(loopTimeMetricId);
 *
 * Then just use
 * pm.getSummary() to get a string with pre-formatted data
 * or
 * performanceMetric.logSummary() to log directly into the DEBUG log
 *
 * You can simultaneously run as many measurements as you like, info about all ended measurements will be present in the pm.getSummary()
 */
const System = require('dw/system/System');
const Logger = require('dw/system/Logger').getLogger('debug');

const CreatePerformanceTimeMetric = function () {
    return {
        measurementsDict: {},
        /**
         * If no measurement with provided id found - create new one;
         * If measurement is currently active - do nothing;
         * @param measurementID
         */
        startMeasurement: function (measurementID) {
            if (!this.measurementsDict[measurementID]) {
                this.measurementsDict[measurementID] = {
                    startDate: new Date(),
                    isActive: true,
                    diffMS: 0
                }
            } else if (!this.measurementsDict[measurementID].isActive) {
                this.measurementsDict[measurementID].isActive = true;
                this.measurementsDict[measurementID].startDate = new Date();
            }
        },
        /**
         * If measurement is not currently active - do nothing;
         * @param measurementID
         */
        endMeasurement: function (measurementID) {
            if (this.measurementsDict[measurementID] && this.measurementsDict[measurementID].isActive) {
                this.measurementsDict[measurementID].isActive = false;
                this.measurementsDict[measurementID].diffMS += new Date() - this.measurementsDict[measurementID].startDate;
            }
        },
        getSummary: function() {
            var summary = ['\n\nTime measurements summary:'];
            for (var measurementID in this.measurementsDict) {
                if (this.measurementsDict.hasOwnProperty(measurementID) && !this.measurementsDict[measurementID].isActive) {
                    summary.push(measurementID + ': ' + this.measurementsDict[measurementID].diffMS + ' ms;')
                }
            }
            return summary.join('\n');
        },
        logSummary: function () {
            Logger.debug(this.getSummary());
        }
    }
};

const CreatePerformanceTimeMetricStub = function () {
    return {
        startMeasurement: function () {},
        endMeasurement: function () {},
        getSummary: function() {},
        logSummary: function () {}
    }
};

let exportFn;
// export stub that does nothing if on "production" type instance
if (System.getInstanceType() === System.PRODUCTION_SYSTEM) {
    exportFn = CreatePerformanceTimeMetricStub;
} else {
    exportFn = CreatePerformanceTimeMetric;
}
exports.CreatePerformanceTimeMetric = exportFn;
